from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class UserProfile(models.Model):

    name = models.CharField(max_length=50)
    birthdate = models.DateField()
    id_line = models.CharField(max_length=10)
    desc = models.TextField()
    photo = models.ImageField(default="default.jpg", blank=True)
    username = models.ForeignKey(
        User, blank=True, null=True, on_delete=models.CASCADE)
    url_profile = models.CharField(max_length=50)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        url = self.url_profile
        return "/accounts/{}/".format(url)
