from django.urls import path, include, re_path
from . import views

app_name = 'accounts'

urlpatterns = [
    path('login/', views.login_view, name='login'),
    path('signup/', views.signup_view, name='signup'),
    path('logout/', views.logout_view, name='logout'),
    path('create-profile/', views.create_view, name='create'),
    re_path(r'^profile-(?P<url_profile>[\w-]+)/$',
            views.profile_view, name='profiles'),
    re_path(r'^(?P<url_profile>[\w-]+)/$', views.profile_view, name='profile')

]
