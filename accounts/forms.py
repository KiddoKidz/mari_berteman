from . import models
from django import forms


class CreateProfile(forms.ModelForm):
    class Meta:
        model = models.UserProfile
        fields = ['name', 'birthdate', 'id_line',
                  'desc', 'photo']
