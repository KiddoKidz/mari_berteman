from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout
from . import forms
from .models import UserProfile

# Create your views here.


def signup_view(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect("accounts:create")
    else:
        form = UserCreationForm()
    return render(request, 'accounts/signup.html', {'form': form})


def login_view(request):
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)

        if form.is_valid():
            # log in the user
            user = form.get_user()
            profile = UserProfile.objects.get(username=user)
            url_final = profile.get_absolute_url()
            login(request, user)
            return redirect(url_final)
    else:
        form = AuthenticationForm()
    return render(request, 'accounts/login.html', {'form': form})


def logout_view(request):
    if request.method == 'POST':
        logout(request)
        return redirect('home')
    logout(request)
    return redirect('home')


def profile_view(request, url_profile):
    if request.method == "GET":
        if 'profile-' not in url_profile:
            url_profile = 'profile-' + url_profile
        prof = UserProfile.objects.get(url_profile=url_profile)
        return render(request, 'accounts/profile_show.html', {'profile': prof})


def create_view(request):
    if request.method == 'POST':
        form = forms.CreateProfile(request.POST, request.FILES)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.username = request.user
            user = request.user
            instance.url_profile = 'profile-' + str(user)
            instance.save()
            prf = UserProfile.objects.get(username=request.user)
            url_final = prf.get_absolute_url()
            return redirect(url_final)
    else:
        form = forms.CreateProfile()
    return render(request, 'accounts/profile_setup.html', {'form': form})
