from django.urls import path, include
from . import views

app_name = 'friends'

urlpatterns = [
    path('', views.friend_list),
    path('list/', views.friend_list, name='add_friend'),

]
