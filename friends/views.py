from django.shortcuts import render
from accounts.models import UserProfile

# Create your views here.


def friend_list(request):
    friends = UserProfile.objects.all()
    friend = friends.get(username=request.user)
    url = friend.url_profile
    return render(request, 'friends/add_friend.html', {'friends': friends, 'url': url})
